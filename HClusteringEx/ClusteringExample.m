% This script demonstrates the hierarchical clustering algorithm on a set of
% artificially generated data points. Data points are considered to be
% sampled from three different multivariate distributions identified by the
% parameters MU1, MU2, MU3 and SIGMA1, SIGMA2, SIGMA3.

%Initialize workspace.
clc
clear all



% Load Feature Maps 
load('Feature1.mat', 'Feature1');
load('Feature2.mat', 'Feature2');
Feature1 = Feature1';
Feature2 = Feature2';


% Define the parameters of the multivariate gaussian distributions.


% Sample equal number of points from both distributions.
% Let N be the number of points to be sampled.
N = 83;
X1 = [Feature1(:,1), Feature2(:,1)];
X2 = [Feature1(:,2), Feature2(:,2) ];
X3 = [Feature1(:,3), Feature2(:,3) ];
% Store both sets of points in a single matrix.
X = [X1;X2;X3];

% Plot the labeded data points

figure('Name','Labeled Data Popints')
hold on
plot(X1(:,1),X1(:,2),'*r','LineWidth',1.4);
plot(X2(:,1),X2(:,2),'*b','LineWidth',1.4);
plot(X3(:,1),X3(:,2),'*g','LineWidth',1.4);
xlabel('Feature2');
ylabel('Feature1');
grid on
hold off

%Plot the unlabaled data points.
figure('Name','Unlabeled Data Popints')
hold on
plot(X(:,1),X(:,2),'*k','LineWidth',1.4);
xlabel('Feature1');
ylabel('Feature2');
grid on
hold off

% Create the hierarchical clustering dendrogram.
Y = pdist(X,'euclidean');
Z = linkage(Y,'ward');
figure('Name','Hierarchical Dendrogram')
[H,T] = dendrogram(Z,0,'colorthreshold','default');
set(H,'LineWidth',2)

% Cluster data in three clusters.
T = clusterdata(X,'linkage','ward','distance','euclidean',3);
figure('Name','Identified Clusters')
scatter(X(:,1),X(:,2),25,T,'fill')
grid on