function transformed_block = preprocess_image( block_struct )
%PREPROCESS_IMAGE Summary of this function goes here
%   Detailed explanation goes here

% block_size = block_struct.blockSize;
% block_loc = block_struct.location;
% img_size  = block_struct.imageSize;
img = block_struct.data;

% turn the gray scale (0.0 ~ 1.0) image 
% to a binary Black&White image (only 0 & 1)
threshold_level = 0.1;
img_sample = im2bw(img, threshold_level);

% extract from the image the coordinates of the pixels
% which belong to the object ( pixel with value = 1)
[row col] = find(img_sample==1);
% the rows of the image matrix map to to x-axis on the xy-plane
% and the columns to the y-axis
coords = [col row];

% find the centroid point ( the mean linear combination of the object's
% pixels)
mean_coords = mean(coords);
% find the PCA components , the new axis where the object has the most
% variance ( rot matrix has the eigenvectors and latent the eigenvalues)
[rot, sc, latent] = pca(coords);

% create the translate to centroid point trnasformation
translate_matrix = [1 0 0;0 1 0; -mean_coords 1];
% the rotate to PCA componet axis rotate
rot_matrix = [rot * [0 -1;1 0], [0;0]; [0 0 1]];
% and the scale transform to max variances and a factor b
%b = max(latent);
b= 4.5/sqrt(max(latent));
scale_matrix = diag([b b 1]);

% get transform object from the matrices
transform = affine2d(translate_matrix*rot_matrix*scale_matrix);
% transform = affine2d(translate_matrix*rot_matrix);
% get a reference object of the original image
Rout = imref2d(size(img));
% change the WorldSize
Rout.XWorldLimits(2) = Rout.XWorldLimits(2)-mean_coords(1);
Rout.YWorldLimits(2) = Rout.YWorldLimits(2)-mean_coords(2);
Rout.XWorldLimits(1) = Rout.XWorldLimits(1)-mean_coords(1);
Rout.YWorldLimits(1) = Rout.YWorldLimits(1)-mean_coords(2);
% change the frame to 32x32 pixels
Rout.ImageSize = [16 16];

% Apply transformation
transformed_block = imwarp(img,transform,'OutputView',Rout);
transformed_block = filter2(0.5*[0.25 0.5 0.25;0.5 1 0.5;0.25 0.5 0.25], transformed_block);
end

