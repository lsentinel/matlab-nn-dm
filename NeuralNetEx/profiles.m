function profiles = profiles( image, threshold )
%RADIAL_PROFILES Summary of this function goes here
%   Detailed explanation goes here
[x,y] = center_of_mass(image);



upper_half_inner_profile = zeros(1,8);
upper_half_outer_profile = zeros(1,8);
lower_half_inner_profile = zeros(1,8);
lower_half_outer_profile = zeros(1,8);


left_half_inner_profile = zeros(1,8);
left_half_outer_profile = zeros(1,8);
diagonal_inner_profiles = zeros(1,4);
diagonal_outer_profiles = zeros(1,4);


min_upper_inner = 16;
max_upper_inner = 0;
min_upper_outer = 16;
max_upper_outer = 0;

min_lower_inner = 16;
max_lower_inner = 0;
min_lower_outer = 16;
max_lower_outer = 0;

for j = 1:8
       img_j = (j-1)*2 + 1;
       img_j_end = (j-1)*2 +2;
        inner_distance = zeros(2,2);
        outer_distance = zeros(2,2);
        for pxl_j = img_j:img_j_end
            idx = mod(pxl_j,2) + 1;
            crossings = 0;
            first_crossing = 0;
            distance = 0;

            pixel = get_pixel( image , x , pxl_j , threshold );
            if ( pixel == 1 )
                inner_distance(idx,1) = distance;
                first_crossing = 1;
            end

            for pxl_i = x:-1:1
                previous_pixel = pixel;

                pixel = get_pixel( image , pxl_i , pxl_j , threshold );

                if ( previous_pixel ~= pixel )                    
                    crossings = crossings + 1;            
                    if( crossings == 1 && first_crossing == 0)
                        inner_distance(idx,1) = distance;
                    end  
                        outer_distance(idx,1) = distance;
                end 
                distance = distance + 1;
            end
            outer_distance(idx,1) = outer_distance(idx,1);
            inner_distance(idx,1) = inner_distance(idx,1);

            crossings = 0;
            first_crossing = 0;
            distance = 0;

            pixel = get_pixel( image , x , pxl_j , threshold );
            if ( pixel == 1 )
                inner_distance(idx,2) = distance;
                first_crossing = 1;
            end
            for pxl_i = x:16
                previous_pixel = pixel;

                pixel = get_pixel( image , pxl_i , pxl_j , threshold );

                if ( previous_pixel ~= pixel )                    
                    crossings = crossings + 1;            
                    if( crossings == 1 && first_crossing == 0)
                        inner_distance(idx,2) = distance;
                    end  
                        outer_distance(idx,2) = distance;
                end 
                distance = distance + 1;
            end
            outer_distance(idx,2) = outer_distance(idx,2);
            inner_distance(idx,2) = inner_distance(idx,2);
        end
        upper_half_inner_profile(j) = (inner_distance(1,1) + inner_distance(2,1))/2;
        upper_half_outer_profile(j) = (outer_distance(1,1) + outer_distance(2,1))/2;
        lower_half_inner_profile(j) = (inner_distance(1,2) + inner_distance(2,2))/2;
        lower_half_outer_profile(j) = (outer_distance(1,2) + outer_distance(2,2))/2;

        if( upper_half_inner_profile(j) < min_upper_inner )
            min_upper_inner = upper_half_inner_profile(j);
        end 
        if( upper_half_inner_profile(j) > max_upper_inner )
            max_upper_inner = upper_half_inner_profile(j);
        end

        if( upper_half_outer_profile(j) < min_upper_outer )
            min_upper_outer = upper_half_outer_profile(j);
        end
        if( upper_half_outer_profile(j) > max_upper_outer )
            max_upper_outer = upper_half_outer_profile(j);
        end

        if( lower_half_inner_profile(j) < min_lower_inner )
            min_lower_inner = lower_half_inner_profile(j);
        end
        if( lower_half_inner_profile(j) > max_lower_inner )
            max_lower_inner = lower_half_inner_profile(j);
        end

        if( lower_half_outer_profile(j) < min_lower_outer )
            min_lower_outer = lower_half_outer_profile(j);
        end
        if( lower_half_outer_profile(j) > max_lower_outer )
            max_lower_outer = lower_half_outer_profile(j);
        end
end

upper_inner_portion = zeros(1,8);
upper_outer_portion = zeros(1,8);
lower_inner_portion = zeros(1,8);
lower_outer_portion = zeros(1,8);

for j = 1:8
upper_inner_portion(j) = (upper_half_inner_profile(j) - min_upper_inner)/(max_upper_inner - min_upper_inner);
upper_outer_portion(j) = (upper_half_outer_profile(j) - min_upper_outer)/(max_upper_outer - min_upper_outer);
lower_inner_portion(j) = (lower_half_inner_profile(j) - min_lower_inner)/(max_lower_inner - min_lower_inner);
lower_outer_portion(j) = (lower_half_outer_profile(j) - min_lower_outer)/(max_lower_outer - min_lower_outer);
end


profiles = [upper_inner_portion;upper_outer_portion;lower_inner_portion;lower_outer_portion];

end

