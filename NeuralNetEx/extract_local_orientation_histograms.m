function local_bins = extract_local_orientation_histograms( block_struct )
%EXTRACT_LOCAL_ORIENTATION_HISTOGRAMS Summary of this function goes here
%   Detailed explanation goes here
R_mag = block_struct.data(:,:,1);  % gradient magnitudes matrix
R_angle = block_struct.data(:,:,2); % gradient angles matrix
block_size = block_struct.blockSize;

% find coordinates in positions of non-zero R_mag values
[i,j] = find(R_mag > 0);

pi_4 = pi/4;
pi_2 = 2*pi_4;
pi_3_4 = 3*pi_4;

% initialize the histogram bins ( one bin for each direction)
local_orientation_bins = zeros(1,8,'double');

% local orientations partitioning
% loop through each non-zero gradient vector 
% and find the bin it belongs
for k =  1:size(i)
    i_k = i(k); % row of non-zero magnitude in magnitude matrix
    j_k = j(k); % column of non-zero magnitude in magnitude matrix
   
    angle = R_angle(i_k,j_k);
    grad_mag = R_mag(i_k,j_k);
    
    if ((0 < angle) && (angle <= pi_4))
        local_orientation_bins(1) = local_orientation_bins(1) + grad_mag;
    elseif ((pi_4 < angle) && (angle <= pi_2))
        local_orientation_bins(2) = local_orientation_bins(2) + grad_mag;
    elseif ((pi_2 < angle) && (angle <= pi_3_4))
        local_orientation_bins(3) = local_orientation_bins(3) + grad_mag;
    elseif ((pi_3_4 < angle) && (angle <= pi))
        local_orientation_bins(4) = local_orientation_bins(4) + grad_mag;
    elseif ((-pi < angle) && (angle <= -pi_3_4))
        local_orientation_bins(5) = local_orientation_bins(5) + grad_mag;
    elseif ((-pi_3_4 < angle) && (angle  <= -pi_2))
        local_orientation_bins(6) = local_orientation_bins(6) + grad_mag;
    elseif ((-pi_2 < angle) && (angle <= -pi_4))
        local_orientation_bins(7) = local_orientation_bins(7) + grad_mag;
    else % -pi_4 < angle && angle =< 0
        local_orientation_bins(8) = local_orientation_bins(8) + grad_mag;
    end
end

local_bins = local_orientation_bins;

end

