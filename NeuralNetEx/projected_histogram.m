function histogram_buckets = projected_histogram( image , type , threshold_color)
%PROJECTED_HISTOGRAM Summary of this function goes here
%   Detailed explanation goes here
histogram_buckets = zeros(1,8);


if ( type == 'v' )
    
    max_vhist = 0;
    min_vhist = 32;
    for i = 1:8
        img_i = (i-1)*2 + 1;
        img_i_end = (i-1)*2 +2;
        for pxl_i = img_i:img_i_end
            for pxl_j = 1:16
                if(image(pxl_i,pxl_j) > threshold_color)
                    histogram_buckets(i) = histogram_buckets(i) + 1;
                end
            end
        end
        
        if ( histogram_buckets(i) > max_vhist )
            max_vhist = histogram_buckets(i);
        end
        
        if ( histogram_buckets(i) < min_vhist )
            min_vhist = histogram_buckets(i);
        end
    end
    for i = 1:8
        histogram_buckets(i) = (histogram_buckets(i) - min_vhist)/(max_vhist - min_vhist);
    end
elseif ( type == 'h' )
    max_hhist = 0;
    min_hhist = 32;
    for j = 1:8
        img_j = (j-1)*2 + 1;
        img_j_end = (j-1)*2 +2;
        for pxl_j = img_j:img_j_end
            for pxl_i = 1:16
                if(image(pxl_i,pxl_j) > threshold_color)
                    histogram_buckets(j) = histogram_buckets(j) + 1;
                end
            end
        end
        
        if ( histogram_buckets(j) > max_hhist )
            max_hhist = histogram_buckets(j);
        end
        
        if ( histogram_buckets(j) < min_hhist )
            min_hhist = histogram_buckets(j);
        end
    end
    for j = 1:8
        histogram_buckets(j) = (histogram_buckets(j) - min_hhist)/(max_hhist - min_hhist);
    end
end
end

