% Clear all existing variables.
clear all

% Load hand written digits images from file usps_all.mat in variable data.
load('usps_all.mat');
% Matrix data is 256 x 1100 x 10 , 3 - dimensional array storing a number
% of 1100 (16 x 16 pixels) images for each digit.
% Keep only those images corresponding to the hand written versions of 0
% and 1.
%X = [data(:,:,1)';data(:,:,10)'];


samples = zeros(16*550,4*16, 'double');

% Put every image reshaped in a large grid of images with 550 row images
% and 4 column images ( first 2 colums for Class0 images, last 2 columns
% for Class1 images)
for i = 1:1100
     sampleClass0 = im2double(reshape(data(:,i,10),16,16));
     sampleClass1 = im2double(reshape(data(:,i,1),16,16)); 
     block_col = floor((i-1)/550); % from 0 ~ 1
     block_row = mod(i-1,550); % from 0 ~ 549

     pixels_row_range = (block_row*16+1):(block_row*16+16);
     pixels_col_range = (block_col*16+1):(block_col*16+16);  
     pixels_col_range_class1 = pixels_col_range + 32;
     
     samples(pixels_row_range,pixels_col_range) = sampleClass0;
     samples(pixels_row_range,pixels_col_range_class1) = sampleClass1;
end


imtool(samples);



new_samples = blockproc(samples,[16 16],@preprocess_image,'UseParallel',true);
features_map = blockproc(new_samples,[16 16],@extract_features,'UseParallel',true);
features_map = [features_map(:,1:128);features_map(:,129:256);features_map(:,257:384);features_map(:,385:512)];
save('features_map.mat','features_map');

imtool(features_map);



% transformed_img = preprocess_image(img_sample);
% figure, imshow(transformed_img)


% for k = 0:(n-1)
%   img_rep((k*20+1):(k*20+20),:)= [horizontal_space;reshape(data(:,k+1,10),16,16),vertical_space,reshape(data(:,k+1,1),16,16);horizontal_space];
% end
% figure
%   imshow(img_rep,'Border','tight', 'InitialMagnification', 'fit')

% threshold = 5;
% I0 = reshape(data(:,50,10),16,16);
% I1 = zoning_density(I0);
% vhist  = projected_histogram(I0,'v',threshold);
% hhist = projected_histogram(I0,'h',threshold);
% %vhist2 = projected_histograms2(I0,'v',threshold);
% %hhist2 = projected_histograms2(I0,'h',threshold);
% profiles = profiles(I0,threshold);
% crossings = crossings(I0,threshold);
% figure
% hold on
% subplot(8,2,1),imshow(I1)
% subplot(8,2,2),imshow(I0)
% subplot(8,2,3),bar(vhist)
% subplot(8,2,4),bar(hhist)
% %subplot(8,2,5),bar(vhist2)
% %subplot(8,2,6),bar(hhist2)
% % Crossings
% subplot(8,2,7),bar(crossings(1,:))
% %subplot(8,2,8),bar(crossings(2,:))
% %Boundary Distances
% % subplot(8,2,9),bar(crossings(3,:))
% % subplot(8,2,10),bar(crossings(4,:))
% % subplot(8,2,11),bar(crossings(5,:))
% % subplot(8,2,12),bar(crossings(6,:))
% subplot(8,2,9),bar(profiles(5,:))
% subplot(8,2,10),bar(profiles(6,:))
% subplot(8,2,11),bar(profiles(7,:))
% subplot(8,2,12),bar(profiles(8,:))
% subplot(8,2,13),bar(profiles(1,:))
% subplot(8,2,14),bar(profiles(2,:))
% subplot(8,2,15),bar(profiles(3,:))
% subplot(8,2,16),bar(profiles(4,:))
% % plot3(Y(1,[1:1:1100]),Y(2,[1:1:1100]),Y(3,[1:1:1100]),'*r')
% % hold on
% % plot3(Y(1,[1101:1:2200]),Y(2,[1101:1:2200]),Y(3,[1101:1:2200]),'*g')
% % grid on
% % hold off
