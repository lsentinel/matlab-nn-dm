function histogram_buckets = projected_histograms2( image, type , threshold_color)
%PROJECTED_HISTOGRAMS2 Summary of this function goes here
%   Detailed explanation goes here

histogram_buckets = zeros(1,16);


if ( type == 'v' )
   max_vhist = 0;
    min_vhist = 16;
    for pxl_i = 1:16
        for pxl_j = 1:16
            if(image(pxl_i,pxl_j) > threshold_color)
                    histogram_buckets(pxl_i) = histogram_buckets(pxl_i) + 1;
            end
        end

        if ( histogram_buckets(pxl_i) > max_vhist )
            max_vhist = histogram_buckets(pxl_i);
        end
        
        if ( histogram_buckets(pxl_i) < min_vhist )
            min_vhist = histogram_buckets(pxl_i);
        end
    end
    for i = 1:16
        histogram_buckets(i) = (histogram_buckets(i) - min_vhist)/(max_vhist - min_vhist);
    end
elseif ( type == 'h' )
    max_hhist = 0;
    min_hhist = 16;
	for pxl_j = 1:16
        for pxl_i = 1:16
            if(image(pxl_i,pxl_j) > threshold_color)
                    histogram_buckets(pxl_j) = histogram_buckets(pxl_j) + 1;
            end
        end
        histogram_buckets(pxl_j) = histogram_buckets(pxl_j) / 16;
        
        if ( histogram_buckets(pxl_j) > max_hhist )
            max_hhist = histogram_buckets(pxl_j);
        end
        
        if ( histogram_buckets(pxl_j) < min_hhist )
            min_hhist = histogram_buckets(pxl_j);
        end
    end
    for j = 1:16
        histogram_buckets(j) = (histogram_buckets(j) - min_hhist)/(max_hhist - min_hhist);
    end
end

end

