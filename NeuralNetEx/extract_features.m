function features_vector = extract_features( block_struct )
%PREPROCESS_IMAGE Summary of this function goes here
%   Detailed explanation goes here

% block_size = block_struct.blockSize;
% block_loc = block_struct.location;
% img_size  = block_struct.imageSize;
img = block_struct.data;
transformed_img = img;

% threshold_level = 0.01;

% Thin the object
%transformed_img = im2bw(img, threshold_level);
% transformed_img = bwmorph(transformed_img,'thin');

% transformed_img = filter2(4*[0.25 0.5 0.25;0.5 1 0.5;0.25 0.5 0.25], transformed_img);
% Rx = filter2([0 0 0;0 1 0;0 0 -1],transformed_img);
% Ry = filter2([0 0 0;0 0 1;0 -1 0],transformed_img);
% transformed_img = Ry;

% 
% new_img = zeros(16,16,'double'); 
% new_img = transformed_img;
% while(any(any(transformed_img)))
% eroded_img = imerode(transformed_img,ones(3,3));
% new_img = new_img + eroded_img;
% transformed_img = eroded_img;
% end
% x = max(max(new_img));
% transformed_img = new_img./x;

sobel_map = extract_sobel_map(transformed_img);

feature = blockproc(sobel_map,[4 4],@extract_local_orientation_histograms);
features_vector = [feature(1,:) feature(2,:) feature(3,:) feature(4,:)];

end

