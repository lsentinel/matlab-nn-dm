% Clear all existing variables.
clear all
% Load hand written digits images from file usps_all.mat in variable data.
load('features_map.mat');

features_map = features_map'; % transpose


% Set the training patterns matrix for the MLP object.
P = [features_map(:,1:1000) features_map(:,1101:2100)];
% Set the target vector corresponding to the training patterns stored in P.
T = [zeros(1,1000),ones(1,1000)];

independed_set_features =  [features_map(:,1001:1100) features_map(:,2101:2200)];
independed_set_targets = [zeros(1,100,'uint32'),ones(1,100,'uint32')];

% Plot the training data points to be fed within the perceptron.
% figure('Name','Training Data Points')
% hold on
% i=45
% 
% scatter3(Class0TrainFeatures(:,i),Class0TrainFeatures(:,i+1),Class0TrainFeatures(:,i+2),25,'r');
% 
% scatter3(Class1TrainFeatures(:,i),Class1TrainFeatures(:,i+1),Class1TrainFeatures(:,i+2),25,'g');
% xlabel('x1');
% ylabel('y1');
% grid on
% hold off

% Set the perceptron object for the binary classification problem.
net = patternnet([30 60]);
% best training functions for pattern recognition trainscg or trainrp
% patternnet deafults to trainscg

% Configure and Initialize Network.
net = configure(net,P,T);

% Set perceptron training parameters.
net.trainParam.epochs = 400;
net.trainParam.goal = 0.0;

% net.trainParam.min_grad = 1e-5;
% net.trainParam.max_fail = 6;

% Train perceptron.
[net,tr] = train(net,P,T);
%Plot the training performance of the network object.
plotperform(tr);




% Get network predictions on the independed set.
independed_predictions = sim(net,independed_set_features);
% Estimate the difference between predicted and actual labels.
DiffPredictions = uint32(independed_set_targets) - uint32(independed_predictions);
% Estimate the percentage of correctly classified training patterns.
CorrectTrainPercentage = double(length(find(DiffPredictions==0))) / (2*100)
[independed_predictions' independed_set_targets']
% Get the trained perceptron optimal weight vector (W) and bias term (B).
% W = net.IW{1,1};
% B = net.b{1};

% % Estimate the boundary between the two classes.
% x1 = [R(1,1):1:R(1,2)];
% x2 = - (W(1)/W(2))*x1 - (B/W(2));
% 
% % Plot training data points and corresponding boundary.
% % Plot the training data points to be fed within the perceptron.
% figure('Name','Training Data Points and Boundary')
% hold on
% plot(C1Train(:,1),C1Train(:,2),'*r','LineWidth',1.4);
% plot(C2Train(:,1),C2Train(:,2),'*g','LineWidth',1.4);
% plot(x1,x2,'-k','LineWidth',1.5);
% xlabel('x1');
% ylabel('x2');
% grid on
% hold off

% Tc = ind2vec(uint64(T)+1)
% net2 = newpnn(P,T);
% Y = sim(net2,P);
% Yc = vec2ind(Y)-1

