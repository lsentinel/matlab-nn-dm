function map = extract_sobel_map( img )
%EXTRACT_SOBEL_MASK Summary of this function goes here
%   Detailed explanation goes here
h = fspecial('sobel');

map = zeros(16,16,2,'double');

h_x = -h';
h_y = h;

Sx = filter2(h_x ,img); % Sx
Sy = filter2(h_y ,img); % Sy

% get magnitudes of all the gradient vectors of the image block
grad_mag = sqrt(Sx .^ 2 + Sy .^ 2); 

% find the average gradient magnitude
threshold_grad_mag = mean2(grad_mag);

% find all the non-zero above-average-magintude gradient vectors 
[i,j] = find(grad_mag >= threshold_grad_mag);

% loop through each non-zero above-average-magintude gradient vector 
% and calculate the final magnitude and angles matrices
for k =  1:size(i)
    i_k = i(k); % row of non-zero magnitude in magnitude matrix
    j_k = j(k); % column of non-zero magnitude in magnitude matrix
       
    x_k = Sx(i_k,j_k); % value of x in non-zero magnitude [i_k,j_k]
    y_k = Sy(i_k,j_k); % value of y in non-zero magnitude [i_k,j_k]
    
    % magnitudes of gradient vector matrix
    map(i_k,j_k,1) = grad_mag(i_k,j_k);
    % angles of gradient vector matrix
    map(i_k,j_k,2) = atan2(y_k,x_k);
end

end

