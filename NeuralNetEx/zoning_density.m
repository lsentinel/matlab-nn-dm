function zone_density_buckets = zoning_density( image )
%ZONING_DENSITY Summary of this function goes here
%   Detailed explanation goes here
threshold_color = 10;
zone_density_buckets = zeros(4,4);

for i = 1:4
    img_i = (i-1)*4 + 1;
    img_i_end = (i-1)*4 + 4;
   for j = 1:4
        img_j = (j-1)*4 + 1;
        img_j_end = (j-1)*4 + 4;
        for pxl_i = img_i:img_i_end
            for pxl_j = img_j:img_j_end
                if(image(pxl_i,pxl_j) > threshold_color)
                    zone_density_buckets(i,j) = zone_density_buckets(i,j) + 1;
                end
            end
        end
        zone_density_buckets(i,j) = zone_density_buckets(i,j) / 16;       
   end
end
end

