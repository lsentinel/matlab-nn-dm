function crossings_distances = crossings( image, threshold )
%CROSSINGS Summary of this function goes here
%   Detailed explanation goes here

horizontal_crossings = zeros(1,16);
vertical_crossings = zeros(1,16);
% horizontal_left_distance = zeros(1,16);
% horizontal_right_distance = zeros(1,16);
% vertical_top_distance = zeros(1,16);
% vertical_bottom_distance = zeros(1,16);


for i = 1:16 
    pixel = 0;
    first_hcrossing = 0;
    for j = 1:16 
                
        previous_pixel = pixel;
        
        if ( image(i,j) > threshold )
            pixel = 1;
        else
            pixel = 0;
        end
        
        if ( previous_pixel ~= pixel )
            horizontal_crossings(i) = horizontal_crossings(i) + 1;
            
            if( horizontal_crossings(i) == 1 )
                first_hcrossing = 1;
            end        
            
            % horizontal_right_distance(i) = 0;
        end
        
        %if ( first_hcrossing == 0 )
        %    horizontal_left_distance(i) = horizontal_left_distance(i) + 1;
        %end      
        
        %horizontal_right_distance(i) = horizontal_right_distance(i) + 1;
    end
    %horizontal_left_distance(i) = horizontal_left_distance(i)/16;
    %horizontal_right_distance(i) = horizontal_right_distance(i)/16;
end

% for j = 1:16 
%     pixel = 0;
%     first_vcrossing = 0;
%     for i = 1:16 
%                 
%         previous_pixel = pixel;
%         
%         if ( image(i,j) > threshold )
%             pixel = 1;
%         else
%             pixel = 0;
%         end
%         
%         if ( previous_pixel ~= pixel )
%             vertical_crossings(j) = vertical_crossings(j) + 1;
%             
%             if( vertical_crossings(j) == 1 )
%                 first_vcrossing = 1;
%             end
%             
%             vertical_bottom_distance(j) = 0;
%         end
%         
%         if ( first_vcrossing == 0 )
%             vertical_top_distance(j) = vertical_top_distance(j) + 1;
%         end 
%         
%         vertical_bottom_distance(j) = vertical_bottom_distance(j) + 1;
%     end
%     vertical_top_distance(j) = vertical_top_distance(j)/16;
%     vertical_bottom_distance(j) = vertical_bottom_distance(j)/16;
% end

%crossings_distances = [horizontal_crossings;vertical_crossings;horizontal_left_distance;horizontal_right_distance;vertical_top_distance;vertical_bottom_distance];
crossings_distances = [horizontal_crossings]
end

