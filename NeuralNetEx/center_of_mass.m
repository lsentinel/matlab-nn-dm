function [ x,y ] = center_of_mass( image )
%CENTER_OF_MASS Summary of this function goes here
%   Detailed explanation goes here
image = double(image) ./ 255;
sum = 0;
center_x = 0;
center_y = 0;

for i = 1:16
    for j = 1:16
        center_x = center_x + image(i,j) * i;
        center_y = center_y + image(i,j) * j;
        sum = sum + image(i,j);
    end
end
x = ceil(center_x/sum);
y = ceil(center_y/sum);

end

